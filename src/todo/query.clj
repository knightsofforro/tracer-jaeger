(ns todo.query
  (:require [todo.database]
            [korma.core :refer :all]
            [clojure.string :as str]
            [opencensus-clojure.trace :refer [span add-tag]]))

(defentity items)

(defn get-todos []
  (span "obtendo todos os registro na base de dados..."
        (let [itens (select items)]
          (add-tag "Retornou?" (if (> (count itens) 0) (str "Sim: " (count itens) " registro(s)") false))
          itens)))

(defn add-todo [title description]
  (insert items
          (values {:title title :description description})))

(defn delete-todo [id]
  (delete items
          (where {:id [= id]})))

(defn update-todo [id title is-complete]
  (update items
          (set-fields {:title title
                       :is_complete is-complete})
          (where {:id [= id]})))

(defn get-todo [id]
  (first
   (select items
           (where {:id [= id]}))))