(ns todo.database
  (:require [korma.db :as korma]))

(def db-connection-info 
    (korma/mysql 
        {:classname "com.mysql.jdbc.Driver"
        :name "menagerie"
        :host "localhost"
        :port "3306"
        :user "root"
        :subname "//localhost:3306/menagerie?useTimezone=true&serverTimezone=UTC&useSSL=false&allowPublicKeyRetrieval=true"
        :password "123456"}))

; set up korma
(korma/defdb db db-connection-info)