(ns todo.handler
  (:require [compojure.core :refer :all]
            [opencensus-clojure.trace :refer [span add-tag make-downstream-headers]]
            [opencensus-clojure.reporting.jaeger]
            [opencensus-clojure.reporting.logging]
            [opencensus-clojure.ring.middleware :refer [wrap-tracing]]
            [compojure.handler :as handler]
            [ring.middleware.json :as middleware]
            [compojure.route :as route]
            [ring.middleware.json :as json]
            [clojure.string :as str]
            [ring.util.response :refer [response]]
            [ring.adapter.jetty :as jetty]
            [opencensus-clojure.reporting.zipkin]
            ;[ring.adapter.jetty :refer [run-jetty]]
            ;[ring-logger-timbre "0.7.5"]
            [todo.query :refer :all]))


(defn get-all-items []
  (span "get-all"
        (let [return (response (get-todos))]
          (add-tag "return" {:return (if (nil? return) "not-found" return)})
          (add-tag "header" {:headers (make-downstream-headers)})
          (add-tag "status" {:status (:status return)}))))

(defn get-all-items-thread []
  (span "get-all-thread"
        (Thread/sleep 5000)
        (let [return (response (get-todos))]
          (add-tag "return" {:return (if (nil? return) "not-found" return)})
          (add-tag "header" {:headers (make-downstream-headers)})
          (add-tag "status" {:status (:status return)}))))

(defn get-item [id]
  (span "get-item"
        (let [return (response (get-todo id))]
          (add-tag "return" {:return (if (nil? return) "not-found" return)})
          (add-tag "header" {:headers (make-downstream-headers)})
          (add-tag "status" {:status (:status return)}))))

(defn add-item [title description]
  (span "add"
          (let [return (response (add-todo title description))]
            (add-tag "return" {:return (if (nil? return) "not-found" return)})
            (add-tag "header" {:headers (make-downstream-headers)})
            (add-tag "status" {:status (:status return)}))))

(defn all-process []
  (get-all-items)
  (get-all-items-thread)
  (get-item 1)
  (add-item "todos" "todos processos"))

(defroutes app-routes
  (GET "/api/todos" []
    (response (get-all-items)))
  (GET "/api/todos/thread" []
    (response (get-all-items-thread)))
  (GET "/api/todos/all" []
    (response (all-process)))
  (GET "/api/todos/:id" [id]
    (response (get-item (Integer/parseInt id))))
  (POST "/api/todos" {:keys [params]}
    (let [{:keys [title description]} params]
      (response (add-item title description))))
  (route/resources "/")
  (route/not-found "Not Found"))

(def app
  (-> (handler/api app-routes)
      (middleware/wrap-json-body)
      (middleware/wrap-json-response)
      (wrap-tracing (fn [req] (-> req :uri (str/replace #"/" "📱"))))))

(defn init []
  (println "Inicializando API...")
  (opencensus-clojure.trace/configure-tracer {:probability 1.0})
  (opencensus-clojure.reporting.logging/report)
  (opencensus-clojure.reporting.jaeger/report "todo-api"))





; (defroutes app-routes
;   (context "/api" [] 
;     (defroutes api-routes
;       (GET  "/" [] (get-all))
;       (context "/api/todo/:id" [] (defroutes api-routes
;                                (GET "/" [] (get-one id))))))
;   (route/not-found "Not Found"))

; (def app
;   (-> (handler/api app-routes)
;       (wrap-tracing)
;       (json/wrap-json-params)
;       (json/wrap-json-response)))

; (defn -main []
;   (println "config jaeger")
;   (opencensus-clojure.trace/configure-tracer)
;   (opencensus-clojure.reporting.logging/report)
;   (opencensus-clojure.reporting.jaeger/report "api6")
;   (span "start-api"
;         {:status 200
;          :headers {"Content-Type" "text/plain"}
;          :body "API executada."}))

; (defn -main []
;   (println "config")
;   (opencensus-clojure.trace/configure-tracer {:probability 1.0})
;   (opencensus-clojure.reporting.logging/report)
;   (opencensus-clojure.reporting.jaeger/report "api4")
;   (span "requisicao4"
;         {:status 200
;          :headers {"Content-Type" "text/plain"}
;          :body "Hello Ring! Am I reloadable?"})
;   ;(run-jetty (logger.timbre/wrap-with-logger app {:printer :no-color}) {:port (Integer/valueOf (or (System/getenv "port") "80"))})
;   (println "fim"))


; (defn teste
;   (span "print" true))

; ; (def app
; ;   (-> (handler/api app-routes)
; ;       (json/wrap-json-params)
; ;       (json/wrap-json-response)))
; (def app
;   (-> (handler/api app-routes)
;       (middleware/wrap-json-body)
;       (middleware/wrap-json-response)
;       (wrap-tracing)))

; (defn -main []
;   (println "main----------")
;   (opencensus-clojure.trace/configure-tracer {:probability 1.0})
;   (opencensus-clojure.reporting.logging/report)
;   (opencensus-clojure.reporting.jaeger/report "http://localhost:14268/api/todo/traces" "api")
;   (teste)
;   )
;  ; (run-jetty (logger.timbre/wrap-with-logger app {:printer :no-color}) {:port (Integer/valueOf (or (System/getenv "port") "80"))}))