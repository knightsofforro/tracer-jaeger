﻿﻿﻿﻿# tracer-jaeger

Estudo sobre Jaeger + Zipkin + OpenCensus em Clojure.

## Running

docker pull jaegertracing/all-in-one

lein ring server

## Jaeger UI

http://localhost:16686

## License

Copyright © 2019 Knights of Forró 💃🎤🎹🎸💃
