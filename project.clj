(defproject todo "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :min-lein-version "2.0.0"
  :dependencies [[org.clojure/clojure "1.10.1"]
                 [compojure "1.5.1"]
                 [ring/ring-core "1.3.2"]
                 [ring/ring-json "0.3.1"]
                 [korma "0.3.0-RC5"]
                 [mysql/mysql-connector-java "5.1.6"]
                 [ring/ring-defaults "0.2.1"]
                 [org.slf4j/log4j-over-slf4j "1.7.14"]
                 [org.slf4j/jul-to-slf4j "1.7.14"]
                 [org.slf4j/jcl-over-slf4j "1.7.14"]
                 [uswitch/opencensus-clojure "0.2.84"]
                 [ring/ring-jetty-adapter "1.2.1"]
                 [io.opencensus/opencensus-exporter-trace-zipkin "0.19.2"]
                 [io.opencensus/opencensus-exporter-trace-jaeger "0.19.2"]
                 [io.opencensus/opencensus-exporter-trace-logging "0.19.2"]]
  :plugins [[lein-ring "0.9.7"]]
  ;:main todo.handler
  :ring {:handler todo.handler/app
         :init todo.handler/init}
  ;:ring {:handler todo.handler/app}
  :profiles
  {:dev {:dependencies [[javax.servlet/servlet-api "2.5"]
                        [ring/ring-mock "0.3.0"]]}})